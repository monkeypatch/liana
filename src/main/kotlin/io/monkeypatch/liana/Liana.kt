package io.monkeypatch.liana

import io.monkeypatch.liana.utils.depends

interface DependencyGraph<Node> {
    /**
     * Returns the set of all nodes in graph.
     */
    fun nodes() : Set<Node>

    /**
     * Returns the set of immediate dependencies of node.
     */
    fun immediateDependencies(node : Node) : Set<Node>

    /**
     * Returns the set of immediate dependents of node.
     */
    fun immediateDependents(node : Node) : Set<Node>

    /**
     * Returns the set of all things which node depends on, directly or transitively.
     */
    fun transitiveDependencies(node : Node) : Set<Node>

    /**
     * Returns the set of all things which any node in node-set depends on, directly or transitively.
     */
    fun transitiveDependenciesSet(nodes : Set<Node>) : Set<Node>

    /**
     * Returns the set of all things which depend upon node, directly or transitively.
     */
    fun transitiveDependents(node : Node) : Set<Node>

    /**
     * Returns the set of all things which depend upon any node in node-set, directly or transitively.
     */
    fun transitiveDependentsSet(nodes : Set<Node>) : Set<Node>

    /**
     * Returns a new graph with a dependency from node to dep ("node depends on dep").
     * Forbids circular dependencies.
     */
    fun depend(node: Node, dep: Node) : DependencyGraph<Node>

    /**
     * Returns a new graph with the dependency from node to dep removed.
     */
    fun removeEdge(node: Node, dep: Node) : DependencyGraph<Node>

    /**
     * Returns a new dependency graph with all references to node removed.
     */
    fun removeAll(node : Node) : DependencyGraph<Node>

    /**
     * Removes the node from the dependency graph without removing it as a
     * dependency of other nodes. That is, removes all outgoing edges from
     * node.
     */
    fun removeNode(node : Node) : DependencyGraph<Node>
}

class MapDependencyGraph<Node> private constructor(
    private val dependencies : Map<Node, Set<Node>> = emptyMap(),
    private val dependents : Map<Node, Set<Node>> = emptyMap()
) : DependencyGraph<Node> {

    companion object {
        operator fun <Node> invoke(): MapDependencyGraph<Node> =
            MapDependencyGraph()
    }

    override fun nodes(): Set<Node> =
        dependencies.filterValues { it.isNotEmpty() }.keys union
                dependents.filterValues { it.isNotEmpty() }.keys

    override fun immediateDependencies(node: Node): Set<Node> =
        dependencies.getOrDefault(node, emptySet())

    override fun immediateDependents(node: Node): Set<Node> =
        dependents.getOrDefault(node, emptySet())

    /**
     * Recursively expands the set of dependency relationships starting
     * at neighbors.get(x), for each x in node-set
     */
    private fun transitive(neighbors : Map<Node, Set<Node>>, nodes : Set<Node>) : Set<Node> {
        tailrec fun inner(unexpanded : Set<Node>, expanded : Set<Node>) : Set<Node> =
            if (unexpanded.isNotEmpty()) {
                val node = unexpanded.first()
                val more = unexpanded.minus(node)
                if (expanded.contains(node)) {
                    inner(more, expanded)
                } else {
                    inner(
                        (neighbors[node]?: emptySet()).plus(more),
                        expanded.plus(node)
                    )
                }
            } else {
                expanded
            }

        return inner(
            neighbors
                .filterKeys { nodes.contains(it) }
                .values
                .fold(initial= emptySet()) { acc, it -> acc union it },
            emptySet()
        )
    }

    override fun transitiveDependencies(node: Node): Set<Node> =
        transitive(dependencies, setOf(node))

    override fun transitiveDependenciesSet(nodes: Set<Node>): Set<Node> =
        transitive(dependencies, nodes)

    override fun transitiveDependents(node: Node): Set<Node> =
        transitive(dependents, setOf(node))

    override fun transitiveDependentsSet(nodes: Set<Node>): Set<Node> =
        transitive(dependents, nodes)

    override fun depend(node: Node, dep: Node) : MapDependencyGraph<Node> {
        if (dep == node || this.depends(dep, node))
            throw IllegalArgumentException("Circular dependency between $node and $dep.")

        return MapDependencyGraph(
            dependencies + mapOf(node to dependencies.getOrDefault(node, emptySet()).plus(dep)),
            dependents + mapOf(dep to dependents.getOrDefault(dep, emptySet()).plus(node))
        )
    }

    override fun removeEdge(node: Node, dep: Node): MapDependencyGraph<Node> =
        MapDependencyGraph(
            dependencies + mapOf(node to dependencies.getOrDefault(node, emptySet()).minus(dep)),
            dependents + mapOf(dep to dependents.getOrDefault(dep, emptySet()).minus(node))
        )

    override fun removeAll(node: Node): MapDependencyGraph<Node> {
        fun removeFromMap(m : Map<Node, Set<Node>>, node : Node) : Map<Node, Set<Node>> =
            m.minus(node).mapValues { (_, value) -> value.minus(node) }

        return MapDependencyGraph(
            removeFromMap(dependencies, node),
            removeFromMap(dependents, node)
        )
    }

    override fun removeNode(node: Node): MapDependencyGraph<Node> {
        return MapDependencyGraph(
            dependencies.minus(node),
            dependents
        )
    }

    override fun toString(): String {
        val ref = Integer.toHexString(System.identityHashCode(this))
        return "io.monkeypatch.liana.MapDependencyGraph@$ref(dependencies=$dependencies, dependents=$dependents)"
    }
}
