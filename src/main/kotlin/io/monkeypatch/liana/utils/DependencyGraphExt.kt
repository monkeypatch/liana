package io.monkeypatch.liana.utils

import io.monkeypatch.liana.DependencyGraph
import java.util.*

/**
 * "True if node is directly or transitively dependent on dep."
 */
fun <Node> DependencyGraph<Node>.depends(node : Node, dep : Node) : Boolean =
    this.transitiveDependencies(node).contains(dep)

/**
 * True if dep is a dependent of node.
 */
fun <Node> DependencyGraph<Node>.dependent(node : Node, dep : Node) : Boolean =
    this.transitiveDependents(node).contains(dep)

/**
 * Returns a topologically-sorted list of nodes in graph.
 */
fun <Node> DependencyGraph<Node>.topoSort() : List<Node> {
    tailrec fun sort(sorted : LinkedList<Node>, graph : DependencyGraph<Node>, todo : Set<Node>) : List<Node> {
        if (todo.isEmpty()) {
            return sorted
        } else {
            val node = todo.first()
            val more = todo.minus(node)
            val deps = graph.immediateDependencies(node)

            tailrec fun inner(deps : Set<Node>, g : DependencyGraph<Node>, add : Set<Node>) : Pair<Set<Node>, DependencyGraph<Node>> =
                if (deps.isNotEmpty()) {
                    val d = deps.first()
                    val newG = g.removeEdge(node, d)
                    if (newG.immediateDependents(d).isEmpty()) {
                        inner(
                            deps.minus(d),
                            newG,
                            add.plus(d)
                        )
                    } else {
                        inner(
                            deps.minus(d),
                            newG,
                            add
                        )
                    }
                } else {
                    Pair(add, g)
                }

            val (add, g) = inner(deps, graph, emptySet())
            val newSorted = LinkedList(sorted)
            newSorted.addFirst(node)
            return sort(
                newSorted,
                g.removeNode(node),
                add union more
            )
        }
    }

    return sort(
        LinkedList(),
        this,
        this.nodes().filter { this.immediateDependents(it).isEmpty() }.toSet()
    )
}

/**
 * True if x comes before y in an ordered collection.
 */
fun <Node> List<Node>.before(x : Node, y : Node) : Boolean {
    tailrec fun inner(coll : List<Node>) : Boolean =
        if (coll.isEmpty()) {
            true
        } else {
            val item = coll.first()
            val more = coll.minus(item)
            when (item) {
                x -> true
                y -> false
                else -> inner(more)
            }
        }
    return inner(this)
}
