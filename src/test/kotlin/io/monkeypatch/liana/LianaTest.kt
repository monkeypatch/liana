package io.monkeypatch.liana

import assertk.assertThat
import assertk.assertions.isEqualTo
import io.monkeypatch.liana.utils.before
import io.monkeypatch.liana.utils.topoSort
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

// building a graph like
//
//       a
//      /|
//     b |
//      \|
//       c
//       |
//       d
//
val g1 = MapDependencyGraph<String>()
    .depend("b", "a")
    .depend("c", "b")
    .depend("c", "a")
    .depend("d", "c")

//      'one    'five
//        |       |
//      'two      |
//       / \      |
//      /   \     |
//     /     \   /
// 'three   'four
//    |      /
//  'six    /
//    |    /
//    |   /
//    |  /
//  'seven
//
val g2 = MapDependencyGraph<String>()
    .depend("two"  , "one")
    .depend("three", "two")
    .depend("four" , "two")
    .depend("four" , "five")
    .depend("six"  , "three")
    .depend("seven", "six")
    .depend("seven", "four")

//               level0
//               / | |  \
//          -----  | |   -----
//         /       | |        \
// level1a level1b level1c level1d
//         \       | |        /
//          -----  | |   -----
//               \ | |  /
//               level2
//               / | |  \
//          -----  | |   -----
//         /       | |        \
// level3a level3b level3c level3d
//         \       | |        /
//          -----  | |   -----
//               \ | |  /
//               level4
//
// ... and so on in a repeating pattern like that, up to level26

val g3 = MapDependencyGraph<String>()
    .depend("level1a", "level0")
    .depend("level1b", "level0")
    .depend("level1c", "level0")
    .depend("level1d", "level0")
    .depend("level2" , "level1a")
    .depend("level2" , "level1b")
    .depend("level2" , "level1c")
    .depend("level2" , "level1d")

    .depend("level3a", "level2")
    .depend("level3b", "level2")
    .depend("level3c", "level2")
    .depend("level3d", "level2")
    .depend("level4" , "level3a")
    .depend("level4" , "level3b")
    .depend("level4" , "level3c")
    .depend("level4" , "level3d")

    .depend("level5a", "level4")
    .depend("level5b", "level4")
    .depend("level5c", "level4")
    .depend("level5d", "level4")
    .depend("level6" , "level5a")
    .depend("level6" , "level5b")
    .depend("level6" , "level5c")
    .depend("level6" , "level5d")

    .depend("level7a", "level6")
    .depend("level7b", "level6")
    .depend("level7c", "level6")
    .depend("level7d", "level6")
    .depend("level8" , "level7a")
    .depend("level8" , "level7b")
    .depend("level8" , "level7c")
    .depend("level8" , "level7d")

    .depend("level9a", "level8")
    .depend("level9b", "level8")
    .depend("level9c", "level8")
    .depend("level9d", "level8")
    .depend("level10", "level9a")
    .depend("level10", "level9b")
    .depend("level10", "level9c")
    .depend("level10", "level9d")

    .depend("level11a", "level10")
    .depend("level11b", "level10")
    .depend("level11c", "level10")
    .depend("level11d", "level10")
    .depend("level12" , "level11a")
    .depend("level12" , "level11b")
    .depend("level12" , "level11c")
    .depend("level12" , "level11d")

    .depend("level13a", "level12")
    .depend("level13b", "level12")
    .depend("level13c", "level12")
    .depend("level13d", "level12")
    .depend("level14" , "level13a")
    .depend("level14" , "level13b")
    .depend("level14" , "level13c")
    .depend("level14" , "level13d")

    .depend("level15a", "level14")
    .depend("level15b", "level14")
    .depend("level15c", "level14")
    .depend("level15d", "level14")
    .depend("level16" , "level15a")
    .depend("level16" , "level15b")
    .depend("level16" , "level15c")
    .depend("level16" , "level15d")

    .depend("level17a", "level16")
    .depend("level17b", "level16")
    .depend("level17c", "level16")
    .depend("level17d", "level16")
    .depend("level18", "level17a")
    .depend("level18", "level17b")
    .depend("level18", "level17c")
    .depend("level18", "level17d")

    .depend("level19a", "level18")
    .depend("level19b", "level18")
    .depend("level19c", "level18")
    .depend("level19d", "level18")
    .depend("level20" , "level19a")
    .depend("level20" , "level19b")
    .depend("level20" , "level19c")
    .depend("level20" , "level19d")

    .depend("level21a", "level20")
    .depend("level21b", "level20")
    .depend("level21c", "level20")
    .depend("level21d", "level20")
    .depend("level22", "level21a")
    .depend("level22", "level21b")
    .depend("level22", "level21c")
    .depend("level22", "level21d")

    .depend("level23a", "level22")
    .depend("level23b", "level22")
    .depend("level23c", "level22")
    .depend("level23d", "level22")
    .depend("level24" , "level23a")
    .depend("level24" , "level23b")
    .depend("level24" , "level23c")
    .depend("level24" , "level23d")

    .depend("level25a", "level24")
    .depend("level25b", "level24")
    .depend("level25c", "level24")
    .depend("level25d", "level24")
    .depend("level26" , "level25a")
    .depend("level26" , "level25b")
    .depend("level26" , "level25c")
    .depend("level26" , "level25d")

internal class LianaTest {
    @Test
    fun transitiveDependencies() {
        assertThat(g1.transitiveDependencies("d"))
            .isEqualTo(setOf("a", "b", "c"))
        assertThat(g2.transitiveDependencies("seven"))
            .isEqualTo(setOf("two", "four", "six", "one", "five", "three"))
    }

    @Test
    fun transitiveDependenciesDeep() {
        assertThat(g3.transitiveDependencies("level24"))
            .isEqualTo(setOf(
                "level0",
                "level1a", "level1b", "level1c", "level1d",
                "level2",
                "level3a", "level3b", "level3c", "level3d",
                "level4",
                "level5a", "level5b", "level5c", "level5d",
                "level6",
                "level7a", "level7b", "level7c", "level7d",
                "level8",
                "level9a", "level9b", "level9c", "level9d",
                "level10",
                "level11a", "level11b", "level11c", "level11d",
                "level12",
                "level13a", "level13b", "level13c", "level13d",
                "level14",
                "level15a", "level15b", "level15c", "level15d",
                "level16",
                "level17a", "level17b", "level17c", "level17d",
                "level18",
                "level19a", "level19b", "level19c", "level19d",
                "level20",
                "level21a", "level21b", "level21c", "level21d",
                "level22",
                "level23a", "level23b", "level23c", "level23d"
            ))
        assertThat(g3.transitiveDependencies("level26"))
            .isEqualTo(setOf(
                "level0",
                "level1a", "level1b", "level1c", "level1d",
                "level2",
                "level3a", "level3b", "level3c", "level3d",
                "level4",
                "level5a", "level5b", "level5c", "level5d",
                "level6",
                "level7a", "level7b", "level7c", "level7d",
                "level8",
                "level9a", "level9b", "level9c", "level9d",
                "level10",
                "level11a", "level11b", "level11c", "level11d",
                "level12",
                "level13a", "level13b", "level13c", "level13d",
                "level14",
                "level15a", "level15b", "level15c", "level15d",
                "level16",
                "level17a", "level17b", "level17c", "level17d",
                "level18",
                "level19a", "level19b", "level19c", "level19d",
                "level20",
                "level21a", "level21b", "level21c", "level21d",
                "level22",
                "level23a", "level23b", "level23c", "level23d",
                "level24",
                "level25a", "level25b", "level25c", "level25d"
            ))
    }

    @Test
    fun transitiveDependents() {
        assertThat(g2.transitiveDependents("five"))
            .isEqualTo(setOf("four", "seven"))
        assertThat(g2.transitiveDependents("two"))
            .isEqualTo(setOf("four", "seven", "six", "three"))
    }

    @Test
    fun noCycles() {
        val exception = assertThrows<IllegalArgumentException> {
            MapDependencyGraph<String>()
                .depend("b", "a")
                .depend("c", "b")
                .depend("a", "c")
        }
        assertEquals("Circular dependency between a and c.", exception.message)
    }

    @Test
    fun noSelfCycles() {
        val exception = assertThrows<IllegalArgumentException> {
            MapDependencyGraph<String>()
                .depend("b", "a")
                .depend("a", "a")
        }
        assertEquals("Circular dependency between a and a.", exception.message)
    }

    @Test
    fun before() {
        val l = listOf("a", "b", "c", "d")
        assertTrue(l.before("a", "b"))
        assertTrue(l.before("b", "c"))
        assertFalse(l.before("d", "c"))
        assertFalse(l.before("c", "a"))
    }

    @Test
    fun topoSort() {
        val sorted = g2.topoSort()
        assertTrue(sorted.before("one", "two"))
        assertTrue(sorted.before("one", "three"))
        assertTrue(sorted.before("one", "four"))
        assertTrue(sorted.before("one", "six"))
        assertTrue(sorted.before("one", "seven"))
        assertTrue(sorted.before("two", "three"))
        assertTrue(sorted.before("two", "four"))
        assertTrue(sorted.before("two", "six"))
        assertTrue(sorted.before("two", "seven"))
        assertTrue(sorted.before("three", "six"))
        assertTrue(sorted.before("three", "seven"))
        assertTrue(sorted.before("four", "seven"))
        assertTrue(sorted.before("five", "four"))
        assertTrue(sorted.before("five", "seven"))
        assertTrue(sorted.before("six", "seven"))
    }
}
